var gulp = require('gulp'),
	util = require('gulp-util'),
	gulpFilter = require('gulp-filter'),
	concat = require('gulp-concat'),
	sass = require('gulp-ruby-sass'),
	uglify = require('gulp-uglify'),
	rename = require('gulp-rename'),
	templateCache = require('gulp-templatecache'),
	wrap = require('gulp-wrap'),
	pipeline = require('multipipe'),
	colors = util.colors,
	log = util.log,
	livereload = require('gulp-livereload'),
  autoprefix = require('gulp-autoprefixer'), 
  notify = require("gulp-notify"),
  bower = require('gulp-bower'),
  bowerSrc = require('gulp-bower-src'),
  es = require('event-stream')


	// NOTE: don't join the template strings, it will break Slush!
	wrapper = '(function(undefined){\n\n<' + '%= contents %>\n}());';

config = {
  bowerDir : 'bower_components',
  sassPath : 'scss'
}

var filter = gulpFilter('**/*.min.js', 'jsfeat/build/jsfeat.js', 'angular-scroll-glue/src/scrollglue.js','!**/locales.min.js','!**/moment-with-locales.min.js', '!**/*lodash.min.js');

gulp.task('bower', function() {
  bowerSrc()
    .pipe(filter)
    .pipe(concat('app-lib.js'))
    .pipe(gulp.dest('./public/js'));
  bowerSrc()
    .pipe( gulpFilter('lodash/dist/lodash.min.js') )
    .pipe(concat('lodash.min.js'))
    .pipe( gulp.dest('./public/js'));
  bowerSrc()
    .pipe( gulpFilter('jsfeat/build/jsfeat.js') )
    .pipe(concat('jsfeat.js'))
    .pipe( gulp.dest('./public/js'));
});

gulp.task('icons', function() { 
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*') 
        .pipe(gulp.dest('./public/fonts')); 
});

gulp.task('min', function() {
	var pipe = pipeline(
		gulp.src(['src/module.js', 'src/routes.js', 'src/**/*.js']),
		concat('app.js'),
		wrap(wrapper),
		gulp.dest('public/js'),
		uglify(),
		rename({
			suffix: '.min'
		}),
		gulp.dest('public/js')
	);

	pipe.on('error', createLogger('min'));
	return pipe;
});
gulp.task('sass', function() {
    var pipe = pipeline(
    sass(config.sassPath, {
      style: 'expanded',
      loadPath: [
        './scss',
         config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
         config.bowerDir + '/fontawesome/scss',
      ],
      "sourcemap=none": true}),
        concat('app.css'),
        gulp.dest('public/css'));
	pipe.on('error', createLogger('sass'));
	return null;
});

gulp.task('css', function() { 
  var vendorCss = gulp.src([
        config.bowerDir + "/dcjs/*.css",
        config.bowerDir + "/angular/*css"]);
  var styleFiles =
    sass(config.sassPath, {
      style: 'expanded',
      loadPath: [
        './scss',
         config.bowerDir + '/bootstrap-sass-official/assets/stylesheets',
         config.bowerDir + '/fontawesome/scss',
      ],
      "sourcemap=none": true});
  return es.concat(vendorCss,styleFiles)
    .pipe(concat('style.css'))
    .pipe(autoprefix('last 2 version'))
    .pipe(gulp.dest('./public/css')); 
});

gulp.task('mocks', function() {
	var pipe = pipeline(
		gulp.src(['mocks/module.js', 'mocks/**/*.js']),
		concat('mocks.js'),
		wrap(wrapper),
		gulp.dest('test')
	);

	pipe.on('error', createLogger('mocks'));
	return pipe;
})


gulp.task('views', function() {
	var pipe = pipeline(
		gulp.src('views/**/*.html'),
		templateCache({
			output: 'views.js',
			strip: 'views',
			moduleName: 'app',
			minify: {
				collapseBooleanAttributes: true,
				collapseWhitespace: true
			}
		}),
		gulp.dest('public/js')
	);

	pipe.on('error', createLogger('views'));
	return pipe;
});

//just serve the client project -- no need to build
gulp.task('serve', function() {
	require('./server');
});

// @see https://github.com/karma-runner/gulp-karma#do-we-need-a-plugin
gulp.task('test', function(done) {
	karma.start({
		configFile: __dirname + '/test/karma.conf.js',
		singleRun: true
	}, done);
});

gulp.task('watch', function() {
	livereload.listen();

	function handleChanges(stream) {
		stream.on('change', livereload.changed);
	}

	handleChanges(gulp.watch('src/**/*.js', ['min']));
	handleChanges(gulp.watch('scss/**/*.scss', ['sass']));
	handleChanges(gulp.watch('views/**/*.html', ['views']));
	handleChanges(gulp.watch('mocks/**/*.js', ['mocks']));
});

gulp.task('build', ['bower',  'sass','icons','css' ,'mocks', 'views'])

gulp.task('default', ['bower',  'sass','icons','css', 'mocks', 'views', 'watch']);

function createLogger(name) {
	return function() {
		var i = arguments.length,
			args = new Array(i);

		while (i--) args[i] = arguments[i];

		args.unshift(colors.red('>>' + name) + ': ');
		log.apply(null, args);
	};
}
