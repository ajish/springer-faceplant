$module.factory('TimelineService', ['$http', 'ROCKSTEADY_CONFIG',
  function($http, ROCKSTEADY_CONFIG) {
    return {
      getTimeline: function(callback) {
        svcUrl = ROCKSTEADY_CONFIG.baseUrl + '/timeline';
        if (ROCKSTEADY_CONFIG.devMode){
          svcUrl = ROCKSTEADY_CONFIG.timelineTestUrl;
        }
        d3.csv(svcUrl, callback);
      },
      getTicks: function(callback) {
        svcUrl = ROCKSTEADY_CONFIG.baseUrl + '/ticks';
        //svcUrl = "http://127.0.0.1:5000" + '/ticks';
        $http.get(svcUrl)
        .success(function(data, status, headers, config) {
          callback(data);
        });  
      }
    };
  }
]);
