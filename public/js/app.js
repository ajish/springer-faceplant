(function(undefined){

var $module = angular.module('app', ['ui.router']);

$module.constant('FACEPLANT_CONFIG', {
  devMode: true 
});

$module.config(['$stateProvider',
	function($stateProvider) {
		var states = {
			'index': {
				url: '',
				templateUrl: '/home.html',
				controller: 'HomeController'
			}
		};

		angular.forEach(states, function(config, name) {
			$stateProvider.state(name, config);
		});
	}
]);
$module.factory('TimelineService', ['$http', 'ROCKSTEADY_CONFIG',
  function($http, ROCKSTEADY_CONFIG) {
    return {
      getTimeline: function(callback) {
        svcUrl = ROCKSTEADY_CONFIG.baseUrl + '/timeline';
        if (ROCKSTEADY_CONFIG.devMode){
          svcUrl = ROCKSTEADY_CONFIG.timelineTestUrl;
        }
        d3.csv(svcUrl, callback);
      },
      getTicks: function(callback) {
        svcUrl = ROCKSTEADY_CONFIG.baseUrl + '/ticks';
        //svcUrl = "http://127.0.0.1:5000" + '/ticks';
        $http.get(svcUrl)
        .success(function(data, status, headers, config) {
          callback(data);
        });  
      }
    };
  }
]);

$module.controller('HomeController', ['$scope', '$http', '$log',
	function($scope, $http, $log, $timeout) {

    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    
    var canvas2 = document.getElementById('canvas2');
    var context2 = canvas2.getContext('2d');
    var canvas3 = document.getElementById('canvas3');
    var context3 = canvas3.getContext('2d');
    var canvas4 = document.getElementById('canvas4');
    var context4 = canvas4.getContext('2d');
    var canvas5 = document.getElementById('canvas5');
    var context5 = canvas5.getContext('2d');
    var img1 = "headshots/s6.jpg";
    var img2 = "headshots/s3.jpg";

    $scope.init = function(){
      $scope.awesome = true;
      var data = {"x":5,"y":5,"amount":15};
      drawDot(data);
    };
    
    function draw_faces(ctx, rects, sc, max, bbf_padding_factor) {
        var on = rects.length;
        if(on && max) {
            jsfeat.math.qsort(rects, 0, on-1, function(a,b){return (b.confidence<a.confidence);})
        }
        var n = max || on;
        n = Math.min(n, on);
        var r;
        var r_data;
        for(var i = 0; i < n; ++i) {
            r = rects[i];
            pad_pixels_x = r.width * bbf_padding_factor;
            pad_pixels_y = r.height * bbf_padding_factor;
            r.x = r.x - pad_pixels_x;
            r.y = r.y - pad_pixels_y;
            r.width = r.width + pad_pixels_x * 2;
            r.height = r.width + pad_pixels_y * 2;
            //$log.info("scale: " + sc);
            r_data = ctx.getImageData((r.x*sc)|0,(r.y*sc)|0,(r.width*sc)|0,(r.height*sc)|0);
            ctx.fillStyle = "#00FF00";
            ctx.fillRect((r.x*sc)|0,(r.y*sc)|0,(r.width*sc)|0,(r.height*sc)|0);
            //ctx.strokeRect((r.x*sc)|0,(r.y*sc)|0,(r.width*sc)|0,(r.height*sc)|0);
        }
        return {"r_data":r_data, "r": r};
    }

    detect_eyes = function( face_data, gray_img, ctxt ){
      jsfeat.imgproc.greyscale(face_data.base_pixels.data, face_data.base_img.width, face_data.base_img.height, gray_img, jsfeat.RGBA2GRAY);
      
      jsfeat.imgproc.equalize_histogram(gray_img, gray_img);
      jsfeat.bbf.prepare_cascade(jsfeat.bbf.face_cascade);

    
    
    }

    detect_face_bbf = function( img, ctx, tw, th, sc ){

        tw = tw || img.width;
        th = th || img.height;
        sc = sc || 1.0;

        w = Math.floor(img.width * sc);
        h = Math.floor(img.height * sc);

        //$log.info("w " + w + " h " + h);

        ctx.drawImage(img, 0, 0, w, h );
        var t_img_data = ctx.getImageData(0, 0, tw, th );
        var image_data = ctx.getImageData(0, 0, w, h );

        var gray_img = new jsfeat.matrix_t( w, h, jsfeat.U8_t | jsfeat.C1_t);
        var code = jsfeat.COLOR_RGBA2GRAY;
        
        jsfeat.imgproc.grayscale(image_data.data, w, h, gray_img, code);
        jsfeat.imgproc.equalize_histogram(gray_img, gray_img);
        jsfeat.bbf.prepare_cascade(jsfeat.bbf.face_cascade);

        var pyr = jsfeat.bbf.build_pyramid(gray_img, 24*2, 24*2, 12);
        var rects = jsfeat.bbf.detect(pyr, jsfeat.bbf.face_cascade);
        rects = jsfeat.bbf.group_rectangles(rects, 1);
        //$log.info(rects);
        var face_data = draw_faces(ctx, rects, w/gray_img.cols, 1, 0);
        var mask_data = ctx.getImageData(0, 0, tw, th );
        var ret_data = {  "img": img,
                          "pixels": t_img_data,
                          "mask_pixels": mask_data,
                          "face_data": face_data };
        return ret_data;
    
    }

    set_haar_options = function( img ){
      
    

    }

    detect_face_haar = function( img, ctx, tw, th, sc ){
      tw = tw || img.width;
      th = th || img.height;
      sc = sc || 1.0;

      var img_u8,work_canvas,work_ctx,ii_sum,ii_sqsum,ii_tilted,edg,ii_canny;
      var classifier = jsfeat.haar.frontalface;
        
      var options = { "min_scale": 2,
                      "scale_factor":1.5,
                      "edges_density":0.13 };   
 
      var w = img.width*sc;
      var h = img.height*sc;

      img_u8 = new jsfeat.matrix_t(w, h, jsfeat.U8_t | jsfeat.C1_t);
      edg = new jsfeat.matrix_t(w, h, jsfeat.U8_t | jsfeat.C1_t);
      work_canvas = document.createElement('canvas');
      work_canvas.width = w;
      work_canvas.height = h;
      work_ctx = work_canvas.getContext('2d');
      ii_sum = new Int32Array((w+1)*(h+1));
      ii_sqsum = new Int32Array((w+1)*(h+1));
      ii_tilted = new Int32Array((w+1)*(h+1));
      ii_canny = new Int32Array((w+1)*(h+1));

      ctx.drawImage(img, 0, 0, w, h);
      var t_img_data = ctx.getImageData(0, 0, tw, th );
      var image_data = ctx.getImageData(0, 0, w, h);
      jsfeat.imgproc.grayscale(image_data.data, w, h, img_u8);

      jsfeat.imgproc.equalize_histogram(img_u8, img_u8);

      jsfeat.imgproc.compute_integral_image(img_u8, ii_sum, ii_sqsum, classifier.tilted ? ii_tilted : null);

      jsfeat.imgproc.canny(img_u8, edg, 10, 50);
      jsfeat.imgproc.compute_integral_image(edg, ii_canny, null, null);

      jsfeat.haar.edges_density = options.edges_density;
      var rects = jsfeat.haar.detect_multi_scale(ii_sum, ii_sqsum, ii_tilted, options.use_canny? ii_canny : null, img_u8.cols, img_u8.rows, classifier, options.scale_factor, options.min_scale);
      rects = jsfeat.haar.group_rectangles(rects, 1);
      //$log.info(rects);
      if (rects.length  < 1){
        return detect_face_bbf( img, ctx, tw, th, sc );
      }
      var face_data = draw_faces(ctx, rects, w/img_u8.cols, 1, 0);
      var mask_data = ctx.getImageData(0, 0, tw, th );
      var ret_data = {  "img": img,
                        "pixels": t_img_data,
                        "mask_pixels": mask_data,
                        "face_data": face_data };
      return ret_data;
    
    }

    rescale_face_data = function( d, sc, tw, th ){

      var work_canvas,work_ctx;
      
      var w = d.img.width*sc;
      var h = d.img.height*sc;

      work_canvas = document.createElement('canvas');
      work_canvas.width = w;
      work_canvas.height = h;
      ctx = context4;//.getContext('2d');

      ctx.drawImage(d.img, 0, 0, w, h);
      var t_img_data = ctx.getImageData(0, 0, tw, th );

      var r = d.face_data.r;
      r.x = r.x*sc;
      r.y = r.y*sc;
      r.width = r.width*sc;
      r.height = r.height*sc;

      var r_data = ctx.getImageData((r.x)|0,(r.y)|0,(r.width)|0,(r.height)|0);
      ctx.fillStyle = "#00FF00";
      ctx.fillRect((r.x)|0,(r.y)|0,(r.width)|0,(r.height)|0);
      var mask_data = ctx.getImageData(0, 0, tw, th );
      var ret_data = {  "img": d.img,
                        "pixels": t_img_data,
                        "mask_pixels": mask_data,
                        "face_data": {"r": r, "r_data": r_data } };

      return ret_data;
    }    


    $scope.loadBaseImage = function(){
      var img = new Image();
      var bbf_padding_factor = 0.05;
      img.src = img1;
      img.onload = function() {
        var base_image_data = detect_face_haar( img, context );
        $scope.loadSrcImage(base_image_data);
      }
    };

    $scope.loadSrcImage = function(bid){
      
      var img = new Image();
      var bbf_padding_factor = 0.05;
      img.src = img2;
      img.onload = function() {
        var sid = detect_face_haar( img, context2, bid.img.width, bid.img.height );
        var scaling_factor = bid.face_data.r.width / sid.face_data.r.width;
        //$log.info("Scaling Factor: "+ scaling_factor);

        sid = rescale_face_data( sid, scaling_factor, bid.img.width, bid.img.height );
        blendImages( bid, sid.pixels, sid.mask_pixels, sid.face_data.r_data, sid.face_data.r, context3, scaling_factor); 
        blendImages( bid, sid.pixels, sid.mask_pixels, sid.face_data.r_data, sid.face_data.r, context3, scaling_factor); 
      };
    };


    function initializeResultCtx(base_pixels, base_size, result_ctx) {
      var result_pixels = result_ctx.getImageData(0, 0, base_size.width, base_size.height);
      for(var i=0; i<result_pixels.data.length; i++) {
        result_pixels.data[i] = 255;
      }
      result_ctx.putImageData(result_pixels, 0, 0);
      result_ctx.putImageData(base_pixels, 0, 0 );
    }



    /*-----------------------------------------
     Blend Images
     g : src_pixels (using mask_pixels)
     f*: base_pixels
     ---> Blend result is result_pixels
    -----------------------------------------*/
    function blendImages(base_image_data, src_pixels, mask_pixels, face_pixels, face_rect, result_ctx, scaling_factor) {

      //result_ctx.scale( scaling_factor, scaling_factor);
      base_img = base_image_data.img;
      base_pixels = base_image_data.pixels;
      base_size = {"width": base_pixels.width, "height": base_pixels.height};
      blend_position_offset = {"x": Math.floor(base_image_data.face_data.r.x) - Math.floor(face_rect.x), "y": Math.floor(base_image_data.face_data.r.y) - Math.floor(face_rect.y) };
      //blend_position_offset = {"x": 0, "y": 0}; 
      initializeResultCtx( base_pixels, base_size,  result_ctx);
      var result_pixels = result_ctx.getImageData(0, 0, base_size.width, base_size.height);
      var dx, absx, previous_epsilon=1.0;
      var cnt=0;



      /*
      $log.info(scaling_factor);
      $log.info(base_img);
      $log.info(base_pixels);
      $log.info(src_pixels);
      $log.info(mask_pixels);
      $log.info(face_pixels);
      $log.info(base_size);
      $log.info(blend_position_offset);
      */
      $log.info(face_pixels);

      do {
        dx=0; absx=0;
        for(var y=1; y<base_size.height-1; y++) {
          for(var x=1; x<base_size.width-1; x++) {
            // p is current pixel
            // rgba r=p+0, g=p+1, b=p+2, a=p+3
            var p = (y*base_size.width+x)*4;

            // Mask area is painted rgba(0,255,0,1.0)
            if(mask_pixels.data[p+0]==0 && mask_pixels.data[p+1]==255 &&
                mask_pixels.data[p+2]==0 && mask_pixels.data[p+3]==255) {

              //$log.info("found a p: " + p);
              var p_offseted = p + 4*(blend_position_offset.y*base_size.width+blend_position_offset.x);

              // q is array of connected neighbors
              var q = [((y-1)*base_size.width+x)*4, ((y+1)*base_size.width+x)*4,
                        (y*base_size.width+(x-1))*4, (y*base_size.width+(x+1))*4];
              var num_neighbors = q.length;

              for(var rgb=0; rgb<3; rgb++) {
                var sum_fq = 0;
                var sum_vpq = 0;
                var sum_boundary = 0;

                for(var i=0; i<num_neighbors; i++) {
                  var q_offseted = q[i] + 4*(blend_position_offset.y*base_size.width+blend_position_offset.x);

                  if(mask_pixels.data[q[i]+0]==0 && mask_pixels.data[q[i]+1]==255 &&
                      mask_pixels.data[q[i]+2]==0 && mask_pixels.data[q[i]+3]==255) {
                    sum_fq += result_pixels.data[q_offseted+rgb];
                  } else {
                    sum_boundary += base_pixels.data[q_offseted+rgb];
                  }

                  if(false && Math.abs(base_pixels.data[p_offseted+rgb]-base_pixels.data[q_offseted+rgb]) >
                    Math.abs(src_pixels.data[p+rgb]-src_pixels.data[q[i]+rgb])) {
                    sum_vpq += base_pixels.data[p_offseted+rgb]-base_pixels.data[q_offseted+rgb];
                  } else {
                    sum_vpq += src_pixels.data[p+rgb]-src_pixels.data[q[i]+rgb];
                  }
                }
                var new_value = (sum_fq+sum_vpq+sum_boundary)/num_neighbors;
                dx += Math.abs(new_value-result_pixels.data[p_offseted+rgb]);
                absx += Math.abs(new_value);
                result_pixels.data[p_offseted+rgb] = new_value;
              }
            }
          }
        }
        cnt++;
        var epsilon = dx/absx;
        if(!epsilon || previous_epsilon-epsilon === 0) break; // convergence
        else previous_epsilon = epsilon;
      } while(true);
      //ctx.globalAlpha=0.2;
      context5.putImageData(result_pixels, 0, 0);

      //result_ctx.putImageData(face_pixels, 0,0);
      //result_ctx.putImageData(mask_pixels, 0, 0);

      alert(cnt+" times iterated.");


    }

    function drawDot(data) {
        context.beginPath();
        context.arc(data.x, data.y, data.amount, 0, 2*Math.PI, false);
        context.fillStyle = "#ccddff";
        context.fill();
        context.lineWidth = 1;
        context.strokeStyle = "#666666";
        context.stroke();  
    }


 
    $scope.init();
	}
]);

}());