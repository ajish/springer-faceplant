Give two headshots,
* find the face in image A
* find the face in image B
* scale image B so that the faces are the same size
* apply a mask to image B showing where the face is
* find the offset between the two faces on image A
* blend face B into face A

# Face Recognition
* try to detect a face using Haar cascades
* if this fails, try to detect a face using BBF
* TODO: filter possible faces using known face point

# Scaling and Adjustment
* using width of faces to scale images
* try to detect eyes to
  1. better scale face regions
  2. capture the angle between the eyes to unrotate face

# Poisson Smoothing
* TODO: Investigate bug on Webkit that requires two calls to putImage
  * This is probably related to some optimization that renders the image before the interpolation is actually done.

